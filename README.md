# Sumário

- [**Request Response**](#request-response)
    - [Prós](#prós)
    - [Contras](#contras)
- [**Pub/Sub**](#pubsub)
    - [Prós](#prós)
    - [Contras](#contras)
- [**Referências**](#referências)


# Request Response

Vamos começar falando um pouco sobre o esquema de request response.

Esse modelo é bem simples. Existe um client, que envia uma request para um servidor e fica aguardando a resposta chegar.

O problema aqui é quando temos uma cadeia de processos acontecendo entre servidores. Ou seja, o client faz uma request para o servidor A, que processa algo e depois precisa encaminhar para o servidor B, que processa algo e depois encaminhar para o servidor C o qual finalmente acaba os processamentos, e agora retorna para B, que retorna para A que finalmente retorna para o client.

```
Client -> A -> B -> C
Client <- A <- B <- C
```

Porém se qualquer coisa der errado em algum dos servidores, todo o processo será uma falha. Ou seja, se C falhou, todo o processo de A e B foi feito por nada.

Agora outro problema seria se B além de enviar para C enviasse para um server D, o qual enviaria para um server E, e assim por diante. Ou seja, a árvore de requests fica muito complexa e o tempo de resposta também e a margem para falhas também.


## Prós

- É bem simples de implementar
- É muito simples de escalar o sistema (criar cópias do servidores e colocar load balancer para eles).

## Contras

- Ruim para múltiplos receptores
- Todos os sistemas acabam por serem muito acoplados
- O client e o servidor precisam estar ativos durante todo o processo
- Falhou em uma parte, falhou em tudo

# Pub/Sub

Como o problema de Request/Response é em casos de encadeamento de servidores. O Pub/Sub vem para resolver isso. E o esquema fica:
- O client envia uma request para um servidor A
- O servidor A envia o pacote para uma fila de processamento
- A fila responde para o servidor A que foi inserido com sucesso
- O servidor A responde para o client
- Agora a fila cuida com a distribuição dos processamentos pelos outros servidores

```
Client -> A -> Fila
Client <- A <- Fila <-> B
               Fila <-> C
               Fila <-> D
```

Veja que o client recebe a resposta bem mais rápido, e agora os serviços trabalham de forma totalmente independentes

## Prós

- Escalável para múltiplos receptores
- Ótimo para micro-serviços
- Os serviços não estão mais altamente acoplados
- Nenhum dos serviços precisa estar funcionando o tempo todo. Pois assim que ele voltar a funcionar ele verá que tem uma nova tarefa na fila e irá executá-la. E o client pode estar até offline, afinal de contas ele nem sabe que essas coisas estão em uma fila.

## Contras

- Fica meio complexo de saber se o processo da fila foi executado com sucesso ou não. e caso ele tenha falhado o que fazer? colocar ele novamente na fila? isso vai acabar gerando duplicata de processos.
- Se um dos servidores que consomem a fila caírem, pode ocorrer um acumulo de serviços na fila, e o servidor não vai dar conta depois.
- Como saber que tem um processo na fila? via ficar mandando request nela toda hora? isso consome banda, ou vai ficar mandando requests com intervalos maiores e vai acabar tendo coisas desatualizadas?

# Referências

- [Publish-Subscribe Architecture (Explained by Example) - Hussein Nasser](https://www.youtube.com/watch?v=O1PgqUqZKTA&list=PLQnljOFTspQUNnO4p00ua_C5mKTfldiYT&index=14)